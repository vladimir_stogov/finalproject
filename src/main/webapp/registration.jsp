<%@ include file="/WEB-INF/directive/taglib.jspf" %>
<!doctype html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>

<div class="form">
<h1>Registration</h1>
<div class="input-form">
<form action="registration" method="post">
<p style="color:white;">Login:</p><input type="text" name="login"><br>
</div>
<div class="input-form">
<p style="color:white;">Email:</p><input type="email" name="email"></br>
</div>
<div class="input-form">
<p style="color:white;">Password:</p><input type="password" name="password"></br>
</div>
<div class="input-form">
<p style="color:white;"><a href="/login">Have account? Login</a></p>
<input type="submit" value="registration">
</div>
</form>
</body>


</html>
