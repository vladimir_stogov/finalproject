<%@ include file="/WEB-INF/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/directive/trans.jspf" %>
<!doctype html>
<html lang="${lang}">
<head>
    <script src="script.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <style type="text/css">
    table {
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
    text-align: left;
    font-size: 20px;
    border-collapse: separate;
    border-spacing: 5px;
    background: #ECE9E0;
    color: #656665;
    border: 5px solid #ECE9E0;
    border-radius: 15px;
    }
    th {
    font-size: 18px;
    padding: 10px;
    }
    td {
    background: #F5D7BF;
    padding: 5px;
    }
    </style>
</head>
<body>
<%@ include file="/WEB-INF/directive/header.jspf" %>
</br>

<input class="form-control" type="text" placeholder="Find book" id="myInput" onkeyup="filtr(0)">

<table border="1" align="center" width="90%" class="table table-striped" id="info-table">
<thead>
        <tr>
            <th scope="col">Book name</th>
            <th scope="col">Author</th>
            <th scope="col">Amount</th>
            <th scope="col">Order</th>
        </tr>
</thead>
<c:forEach var="library" items = "${name}">

<tr>
    <td width="45%"><c:out value="${library.getName()}"/></td>
    <td width="40%"><c:out value="${library.getAuthor()}"/></td>
    <td width="10%"><c:out value="${library.getAmount()}"/></td>
    <td width="5%"><form action="library" method="post">
    <input type="hidden" name="book_id" value ="${library.getId()}">
    <input type="submit" value="Book Order">
    </form></td>
</tr>

</c:forEach>
</table>
<div></br></div>
</body>
</html>