<%@ include file="/WEB-INF/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/directive/trans.jspf" %>
<!doctype html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">

    <style type="text/css">
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            text-align: left;
            font-size: 20px;
            border-collapse: separate;
            border-spacing: 5px;
            background: #ECE9E0;
            color: #656665;
            border: 5px solid #ECE9E0;
            border-radius: 15px;
        }

        th {
            font-size: 18px;
            padding: 10px;
        }

        td {
            background: #F5D7BF;
            padding: 5px;
        }
    </style>

</head>
<body>
<%@ include file="/WEB-INF/directive/header.jspf" %>
<div></br></div>


<table border="1" align="center" width="70%">
        <tr>
            <td width="20%">
                <p align="center"><img src="user.png" width="70%"></p>
            </td>
            <td>
                <p><fmt:message key="user.jsp.login"/>:&nbsp<c:out value="${user.getLogin()}"/><p>
                <p><fmt:message key="user.jsp.email"/>:&nbsp<c:out value="${user.getEmail()}"/><p>
                <p><fmt:message key="user.jsp.role"/>:&nbsp<c:out value="${user.getRole().getName()}"/><p>
    </table>
    </td>
    <tr>
        </table>

<c:forEach var="taken" items="${takens}">
    <table border="1" align="center" width="70%">
        <tr>
            <td width="30%"><c:out value="${taken.getUserLogin()}"/></td>
            <td width="15%"><c:out value="${taken.getBookTitle()}"/></td>
            <td width="15%"><c:out value="${taken.getReturnDate()}"/></td>
        </tr>
    </table>
    </c:forEach>

</body>
</html>