package com.epam.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Penalty {

    private Long id;
    private int penalty_cost;
    private User user;
    private Book book;

}
