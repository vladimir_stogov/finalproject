package com.epam.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Taken {
    private Long id;
    private Long user;
    private Long book;
    private LocalDateTime rеturn;
}
