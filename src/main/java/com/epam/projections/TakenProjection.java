package com.epam.projections;

import com.epam.entities.Book;
import com.epam.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TakenProjection {
    private String userLogin;
    private String bookTitle;

    public String getReturnDate() {
        return returnDate.toString().replace("T"," ");
    }

    private LocalDateTime returnDate;
}
