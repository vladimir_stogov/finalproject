package com.epam.servlets;

import com.epam.dao.impl.UserDAOImpl;
import com.epam.dao.UserDAO;
import com.epam.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class Login extends HttpServlet {

    private UserDAO userDAO = new UserDAOImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User user = userDAO.findByName(login).orElseThrow(RuntimeException::new);

        if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            session.setAttribute("userId", user.getId());
            session.setAttribute("login", login);
            session.setAttribute("password", password);
            session.setAttribute("isLogged", user);
            session.setAttribute("userRole", user.getRole().getName());
            session.setAttribute("lang", "ru");
            resp.sendRedirect("/main");
        }else{
            req.getRequestDispatcher("index.jsp");
            out.print("Log in again");
        }
    out.close();

    }
}
