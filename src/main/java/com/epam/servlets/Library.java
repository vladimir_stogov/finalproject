package com.epam.servlets;

import com.epam.dao.impl.BookDAOImpl;
import com.epam.dao.impl.TakenDAOImpl;
import com.epam.entities.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/library")
public class Library extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookDAOImpl bookDAOImpl = new BookDAOImpl();
        List<Book> bookLists = bookDAOImpl.findALL();
        req.setAttribute("name",bookLists);
        req.setAttribute("author",bookLists);
        req.setAttribute("amount",bookLists);
        req.getRequestDispatcher("library.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TakenDAOImpl takenDAOImpl = new TakenDAOImpl();
        HttpSession session = req.getSession();
        String userId = String.valueOf(session.getAttribute("userId"));
        String bookId = String.valueOf(req.getParameter("book_id"));
        System.out.println(userId);
        System.out.println(bookId);
        takenDAOImpl.orderBook(bookId,userId);
        req.getRequestDispatcher("order.jsp").forward(req, resp);
    }
}
