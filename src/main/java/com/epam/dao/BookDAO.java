package com.epam.dao;

import com.epam.entities.Book;

public interface BookDAO extends GenericDAO<Book,Long> {

}
