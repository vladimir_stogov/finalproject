package com.epam.dao;

import com.epam.entities.Taken;
import com.epam.projections.TakenProjection;

import java.util.List;


public interface TakenDAO extends GenericDAO<Taken, Long> {
    Boolean orderBook(String bookId, String userId);

 List<TakenProjection> findByLogin(String login);
}
