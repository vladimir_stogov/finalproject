package com.epam.dao;

import org.intellij.lang.annotations.Language;

import java.util.Optional;

public interface LanguageDAO extends GenericDAO<Language, Long> {
    Optional<Language> finalByName(String name);
}