package com.epam.dao;

import java.util.List;
import java.util.Optional;

public interface GenericDAO<T, ID> {

    Optional<T> findById(ID id);

    List<T> findALL();

    T create(T entity);

    T update(T entity, ID id);

    void deleteById(ID id);

    void delete(T entity);

}
