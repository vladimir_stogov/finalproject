package com.epam.dao.impl;

import com.epam.dao.UserDAO;
import com.epam.entities.Role;
import com.epam.entities.User;
import com.epam.util.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDAOImpl implements UserDAO {

    public static final String SELECT_FROM_USER_WHERE_USER_LOGIN = "SELECT u.id, u.login, u.password, u.email, r.id as role_id, r.role FROM user as u\n" +
            "join role as r on u.role_id = r.id where u.login = ?";
    public static final String SQL = "SELECT\n" +
            "\tu.login, u.email, u.id, u.password, role.id as role_id,\n" +
            "    role.role\n" +
            "from user u\n" +
            "join role on u.role_id = role.id;";
    private Connection connection;

    @Override
    public Optional<User> findByName(String name) {
        String sql = SELECT_FROM_USER_WHERE_USER_LOGIN;
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User(
                        resultSet.getLong("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        new Role(resultSet.getLong("role_id"),
                        resultSet.getString("role")),
                        resultSet.getString("email"));
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<User> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public List<User> findALL() {
        String sql= SQL;
        User user = null;
        List<User>users = new ArrayList<>();
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                users.add(
                        new User(resultSet.getLong("id"),
                                resultSet.getString("login"),
                                resultSet.getString("password"),
                                new Role(resultSet.getLong("role_id"),
                                        resultSet.getString("role")),
                                resultSet.getString("email")));
            }
            return users;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public UserDAOImpl() {
        this.connection = ConnectionPool.getConnection();
    }

    @Override
    public User create(User entity) {
        String sql = "insert into user(login, email, password ,role_id) values(?,?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getEmail());
            preparedStatement.setString(3, entity.getPassword());
            preparedStatement.setLong(4, 1);
            preparedStatement.executeUpdate();
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public User update(User entity, Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(User entity) {

    }
}
