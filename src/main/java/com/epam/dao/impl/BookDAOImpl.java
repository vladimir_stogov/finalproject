package com.epam.dao.impl;

import com.epam.dao.BookDAO;
import com.epam.entities.Book;
import com.epam.util.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookDAOImpl implements BookDAO {

    public static final String SELECT_FROM_LIBRARY_BOOK = "SELECT * FROM library.book;";
    private Connection connection;

    public BookDAOImpl() {
        this.connection = ConnectionPool.getConnection();
    }

    @Override
    public Optional<Book> findById(Long aLong) {
        return null;
    }

    @Override
    public List<Book> findALL() {
        String sql= SELECT_FROM_LIBRARY_BOOK;
        Book book = null;
        List<Book>books = new ArrayList<>();
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                books.add(
                        new Book(resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("publishing"),
                                new Date(resultSet.getDate("publish_day").getTime()).toLocalDate(),
                                resultSet.getString("author"),
                                resultSet.getInt("amount")));
            }
            return books;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Book create(Book entity) {
        return null;
    }

    @Override
    public Book update(Book entity, Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Book entity) {

    }
}
