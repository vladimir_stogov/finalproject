package com.epam.dao.impl;

import com.epam.dao.RoleDAO;
import com.epam.entities.Role;
import com.epam.util.ConnectionPool;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RoleDAOImpl implements RoleDAO {
    public static final String SELECT_FROM_ROLE = "SELECT * FROM role";
    private Connection connection;

    public RoleDAOImpl() {
        this.connection = ConnectionPool.getConnection();
    }

    @Override
    public Optional<Role> findByName(String name) {
        return Optional.empty();
    }

    @Override
    public Optional<Role> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public List<Role> findALL() {
        String sql = SELECT_FROM_ROLE;

        List<Role> roles = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                roles.add(new Role(
                        resultSet.getLong("id"),
                        resultSet.getString("role")
                ));
            }
            return roles;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public Role create(Role entity) {
        return null;
    }

    @Override
    public Role update(Role entity, Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Role entity) {

    }
}
