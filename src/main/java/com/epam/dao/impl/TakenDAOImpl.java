package com.epam.dao.impl;

import com.epam.dao.TakenDAO;
import com.epam.entities.Taken;
import com.epam.projections.TakenProjection;
import com.epam.util.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

public class TakenDAOImpl implements TakenDAO {
    private Connection connection;

    public TakenDAOImpl() {
        this.connection = ConnectionPool.getConnection();
    }

    @Override
    public Boolean orderBook(String bookId, String userId) {
        String sql = "insert into taken (user_id, book_id, `return`) values (?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, Long.parseLong(userId));
            preparedStatement.setLong(2, Long.parseLong(bookId));
            preparedStatement.setTimestamp(3, new Timestamp(LocalDateTime.now().plusDays(7).toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli()));
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Taken> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public List<Taken> findALL() {
        String sql = "SELECT t.*, b.name, u.login FROM taken t\n" +
                "LEFT JOIN book b on b.id = t.book_id\n" +
                "LEFT JOIN user u on u.id = t.user_id\n" +
                "WHERE u.login = ?;";
        List<Taken> takens = new ArrayList<>();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                takens.add(
                        new Taken(resultSet.getLong("id"),
                                resultSet.getLong("user_id"),
                                resultSet.getLong("book_id"),
                                notDate(resultSet.getTimestamp("return"))));
            }
            return takens;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<TakenProjection> findByLogin(String login) {
        String sql = "SELECT t.*, b.name AS book_title, u.login FROM taken t" +
                " LEFT JOIN book b on b.id = t.book_id" +
                " LEFT JOIN user u on u.id = t.user_id" +
                " WHERE u.login = ?";

        List<TakenProjection> takenProjections = new ArrayList<>();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                takenProjections.add(
                        new TakenProjection(
                                resultSet.getString("login"),
                                resultSet.getString("book_title"),
                                notDate(resultSet.getTimestamp("return"))));
            }
            return takenProjections;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Taken create(Taken entity) {

        return null;
    }

    @Override
    public Taken update(Taken entity, Long aLong) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Taken entity) {

    }

    public LocalDateTime notDate(Timestamp timestamp) {
        if (timestamp != null) {
            return timestamp.toLocalDateTime();
        }
        return null;
    }
}
