package com.epam.dao;

import com.epam.entities.Role;

import java.util.Optional;

public interface RoleDAO extends GenericDAO<Role, Long> {
    Optional<Role> findByName(String name);
    //List<Role> findAll();
}
